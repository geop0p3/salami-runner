extends Node

onready var box = preload("res://Escenas/Obstaculos.tscn")
onready var enemigo1 = preload("res://Escenas/Enemigo1.tscn")
onready var escena = preload("res://Escenas/SalamiScene.tscn")
onready var player = preload("res://SalamiPlayer.tscn")
onready var salami_node = preload("res://SalamiNode.tscn")

func _ready():
	randomize()
	#Escena Salami
	var s = escena.instance()
	add_child(s)
	var b = box.instance()
	add_child(b)
	var e1 = enemigo1.instance()
	add_child(e1)
	var salamiN = salami_node.instance()
	add_child(salamiN)
	var p = player.instance()
	add_child(p)

	
	
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_Timer_timeout():
	var b = box.instance()
	var e1 = enemigo1.instance()
	add_child(b)
	add_child(e1)
	e1.position.y = rand_range(-50, 85)
	e1.position.x = rand_range(250, 700)



func _on_Timer2_timeout():#Escena Salami
	#var s1 = escena.instance()
	#add_child(s1)
	#move_child(s1, 0)
	pass