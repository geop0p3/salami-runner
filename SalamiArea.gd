extends Area2D

onready var platform = 0;
onready var platform_position = [442, 353, 255]

func _ready():
	position.y = platform_position[0]

func _process(delta):
	if Input.is_action_just_pressed("ui_up"):
		if platform >= 0 and platform < 2:
			platform = platform + 1
			position.y = platform_position[platform]
	
	if Input.is_action_just_pressed("ui_down"):
		if platform > 0 and platform <= 2:
			platform = platform - 1
			position.y = platform_position[platform]
	
func _on_SalamiArea_area_entered(area):
	if area.name != "SwordArea":
		get_tree().quit()
