extends Area2D

onready var collision = get_node("CollisionShape2D")

func _ready():
	pass

func _process(delta):
		
	if Input.is_action_just_pressed("ui_select"):
		collision.disabled = false
		visible = true
		
		
	if Input.is_action_just_released("ui_select"):
		collision.disabled = true
		visible = false